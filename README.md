﻿ISI Projekt
===========

Do zbudowania projektu:

    mvn package

Do spakowania źródeł:

    mvn assembly:single

Do uruchomienia projektu (po uprzednim zbudowaniu):

    java -jar solverapp/target/solverapp-1.0.jar

Uruchomienie dla przykładowego problemu:

    cat solverapp/src/main/resources/test-input.json | \
      java -jar solverapp/target/solverapp-1.0.jar

rozwiązanie pojawi się na wyjściu standardowym (także w notacji JSON).

Do uruchomienia projektu w wersji z GUI:

    java -jar solvergui/target/solvergui-1.0.jar

Moduły
------

* `sudoku` - reprezentacja planszy Sudoku
* `solver` - uniwersalny solver używający algorytmu przeszukiwania z tabu
* `solverapp` - aplikacja rozwiązująca Sudoku z użyciem przeszukiwania z tabu
* `solvergui` - jak `solverapp`, ale z GUI
