package pl.edu.pw.elka.stud.isi.solver

trait Problem[T] {
  def initialSolution: T

  def moves: T => Iterator[T => T]

  def goalFunction: T => Problem.Score

  def terminationCondition: T => Boolean
}

object Problem {
  type Score = Int
}
