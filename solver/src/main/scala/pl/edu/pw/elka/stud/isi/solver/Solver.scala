package pl.edu.pw.elka.stud.isi.solver

import collection.{Iterator, TraversableOnce}
import collection.immutable.Queue

object Solver {
  case class Step[T](solution: T, tabu: Queue[T => T],
                     finished: Boolean = false)

  case class NoMove[T]() extends ((T) => T) {
    override def apply(t: T) = t
  }

  def apply[T](problem: Problem[T],
               queueLength: Int,
               limit: Int): TraversableOnce[Step[T]] = {
    val nulls = Iterator.fill[T => T](queueLength)(NoMove[T]()).toSeq
    val start = Step(problem.initialSolution, Queue(nulls: _*))
    Iterator.iterate(start)({ case Step(previous, tabu, _) =>
      val neighbours = problem.moves(previous)
      val nonTabu = neighbours.filterNot(tabu contains _)
      val successors = if (nonTabu.isEmpty) Iterator(NoMove[T]()) else nonTabu
      val chosen = successors.maxBy({ x => problem.goalFunction(x(previous)) })
      Step(chosen(previous), tabu.dequeue._2.enqueue(chosen),
           problem.terminationCondition(previous))
    }).take(limit).takeWhile(!_.finished)
  }
}
