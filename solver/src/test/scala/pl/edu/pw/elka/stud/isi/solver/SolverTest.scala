package pl.edu.pw.elka.stud.isi.solver

import collection.immutable.Queue
import math.abs
import org.junit.Assert.{assertEquals, assertFalse, assertNotNull}
import org.junit.Test

class SolverTest {
  val solver = Solver
  val none = Solver.NoMove[Int]()
  val by10 = { x: Int => x - 10 }
  val by20 = { x: Int => x + 20 }
  val by30 = { x: Int => x - 30 }
  val by50 = { x: Int => x + 50 }
  val problem = new Problem[Int] {
    override def initialSolution = 0
    override def moves = { case _ => Iterator(by10, by20, by30, by50) }
    override def goalFunction = { case x => -abs(x - 100) }
    override def terminationCondition = { case x => x == 100 }
  }
  val inc = { x: Int => x + 1 }
  val dec = { x: Int => x - 1 }
  val allInTabooProblem = new Problem[Int] {
    override def initialSolution = 42
    override def moves = { case _ => Iterator(inc, dec) }
    override def goalFunction = { case x => -abs(x - 49) }
    override def terminationCondition = { case x => x == 49 }
  }

  @Test
  def solutionIsNotNull() {
    assertNotNull(solver(problem, 3, 100))
  }

  @Test
  def solutionIsNeverEmpty() {
    assertFalse(solver(problem, 3, 100).isEmpty)
  }

  @Test
  def expectedStepsAreMade() {
    val solution = solver(problem, 3, 100).toList
    assertEquals(solution, List(
      solver.Step(0,   Queue(none, none, none)),
      solver.Step(50,  Queue(none, none, by50)),
      solver.Step(70,  Queue(none, by50, by20)),
      solver.Step(60,  Queue(by50, by20, by10)),
      solver.Step(30,  Queue(by20, by10, by30)),
      solver.Step(80,  Queue(by10, by30, by50)),
      solver.Step(100, Queue(by30, by50, by20))))
  }

  @Test
  def limitsAreRespected() {
    val solution = solver(problem, 3, 3).toList
    assertEquals(solution, List(
      solver.Step(0,  Queue(none, none, none)),
      solver.Step(50, Queue(none, none, by50)),
      solver.Step(70, Queue(none, by50, by20))))
  }

  @Test
  def doesNotCrashWhenAllNeighboursAreInTabu() {
    val solution = solver(allInTabooProblem, 3, 6).toList
    assertEquals(solution, List(
      solver.Step(42, Queue(none, none, none)),
      solver.Step(43, Queue(none, none, inc)),
      solver.Step(42, Queue(none, inc,  dec)),
      solver.Step(42, Queue(inc,  dec,  none)),
      solver.Step(42, Queue(dec,  none, none)),
      solver.Step(43, Queue(none, none, inc))))
  }
}
