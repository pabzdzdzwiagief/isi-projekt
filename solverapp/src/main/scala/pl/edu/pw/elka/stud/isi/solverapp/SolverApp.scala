package pl.edu.pw.elka.stud.isi.solverapp

import java.io.InputStreamReader
import com.google.gson.Gson
import pl.edu.pw.elka.stud.isi.solver.Solver

object SolverApp extends App {
  val reader = new InputStreamReader(System.in)
  val gson = new Gson
  val sudokuRows = gson.fromJson(reader, classOf[Array[Array[Int]]])
  val problem = SudokuProblem(sudokuRows)
  reader.close()
  for (Solver.Step(solution, tabu, _) <- Solver(problem, 18, 10000)) {
    System.err.println("---------------")
    System.err.println("Plansza Sudoku:")
    System.err.println(solution)
    System.err.println("Jakość rozwiązania:")
    System.err.println(problem.goalFunction(solution))
    if (problem.terminationCondition(solution)) {
      println(gson.toJson(solution.toArray))
    }
  }
}
