package pl.edu.pw.elka.stud.isi.solverapp

import pl.edu.pw.elka.stud.isi.sudoku.Sudoku
import pl.edu.pw.elka.stud.isi.solver.Problem

object SudokuProblem {
  def apply(sudokuRows: Array[Array[Int]]) = new Problem[Sudoku] {
    override def initialSolution : Sudoku = { 
      val s =  new Sudoku(-1, -1)
     s.initialize(sudokuRows)
     return s
    }
    override def moves = {x:Sudoku => x.getMoves }
    override def goalFunction = {x:Sudoku => x.goalFunction }
    override def terminationCondition = { x:Sudoku => x.goalFunction ==0 }
  }
}
