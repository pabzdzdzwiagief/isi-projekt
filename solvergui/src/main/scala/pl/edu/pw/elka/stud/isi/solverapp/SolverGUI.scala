package pl.edu.pw.elka.stud.isi.solverapp

import scala.swing._
import java.awt.{Color, Dimension}
import pl.edu.pw.elka.stud.isi.sudoku.Sudoku
import pl.edu.pw.elka.stud.isi.solver.Solver
import javax.swing.SwingUtilities
import akka.actor.ActorDSL._
import akka.actor.ActorSystem
import pl.edu.pw.elka.stud.isi.solver.Problem

object SolverGUI extends SimpleSwingApplication {
  val fields = new Array[TextField](81)
  val scoreField = new TextField(10)
  implicit val system = ActorSystem("sudoku")
  val guiUpdater = actor("updater")(new Act {
    become {
      case solution: Sudoku =>
        SwingUtilities.invokeLater(new Runnable {
          def run() {
            for (f <- fields if f.foreground != Color.RED) {
              f.foreground = Color.BLACK
              f.repaint
            }
            val firstSwap = solution.firstSwapped
            val secSwap = solution.secondSwapped
            if (firstSwap > 0) {
              val tmp = fields(firstSwap).text
              fields(firstSwap).text = fields(secSwap).text
              fields(secSwap).text = tmp
              fields(firstSwap).revalidate
              fields(secSwap).revalidate
              fields(firstSwap).foreground = Color.BLUE
              fields(secSwap).foreground = Color.BLUE
              fields(firstSwap).repaint
              fields(secSwap).repaint
            }
            else {
              for (i <- 0 to 80) {
                fields(i).text = solution.board(i).toString()
                fields(i).repaint
              }
            }
            scoreField.text = (-solution.goalFunction).toString
            scoreField.repaint
          }
        })
    }
  })
  val solver = actor("solver")(new Act {
    become {
      case problem: Problem[_] =>
        for (Solver.Step(solution, _, _) <- Solver(problem, 20, 5000)) {
          Thread.sleep(100)
          guiUpdater ! solution
        }
    }
  })


  def top = new MainFrame {
    val values = new Array[Int](81)
    for (i <- 0 to 80) values(i) = 0
    title = "Sudoku"
    visible = true;
    contents = new FlowPanel {
      preferredSize = new Dimension(500, 350)
      contents += new GridPanel(3, 3) {
        border = Swing.LineBorder(Color.BLACK, 1)
        for (d <- 0 to 8) {
          contents += new GridPanel(3, 3) {
            border = Swing.LineBorder(Color.BLACK, 1)
            background = Color.WHITE
            for (m <- 0 to 8) {
              val field = new TextField {
                preferredSize = new Dimension(30, 30)
                horizontalAlignment = Alignment.Center
                font = new Font("Arial", 1, 17)
              }
              val ix = (d / 3 * 3 + m / 3) * 9 + (d % 3 * 3 + m % 3)
              fields(ix) = field
              contents += field
            }
          }
        }
      }
      var dupa = Array(
        Array(0, 0, 0, 1, 0, 0, 0, 2, 0),
        Array(1, 0, 9, 0, 0, 0, 0, 5, 0),
        Array(0, 0, 0, 8, 0, 0, 1, 0, 4),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 2, 0, 0, 0, 0, 6, 0),
        Array(4, 0, 0, 0, 0, 0, 0, 0, 8),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 9),
        Array(0, 4, 0, 0, 2, 0, 0, 3, 0),
        Array(8, 0, 3, 0, 9, 0, 0, 0, 1))
      var dup = dupa.flatten
      for (i <- 0 to 80) {
        if (dup(i) != 0) fields(i).text = dup(i).toString
      }
      SwingUtilities.invokeLater(new Runnable {
        def run() {
          for (i <- 0 to 80) {
            val f = fields(i)
            if (f.text != "") {
              f.foreground = Color.RED
              fields(i).repaint
              values(i) = f.text.toInt
            }
          }
        }
      })
      contents += new Label("Ilość konfliktów")
      contents += scoreField
      contents += new Button(Action("Oblicz") {
        val sudokuRows = new Array[Array[Int]](9)
        for (i <- 0 to 8) {
          sudokuRows(i) = values.slice(i * 9, (i + 1) * 9)
        }
        solver ! SudokuProblem(dupa)
      }) {
        preferredSize = new Dimension(100, 20)
        visible = true
      }
    }
  }

}
